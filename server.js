import os from 'node:os';
import process from 'node:process';
import fs from 'node:fs';
import https from 'https';

import express from 'express';
import cors from 'cors';
import formidable from 'formidable';

import uuid4 from 'uuid4';
import axios from 'axios';

const GIT_HASH = process.env.GIT_HASH
console.log(`Git Hash: ${GIT_HASH}`)

const port = process.env.PORT || 3000
const uploadPath = process.env.UPLOAD_PATH || os.tmpdir()
const maxFileSize = 200 * 1024 * 1024 * 1024 // FileSize 200GB

const medialoopsterWebserviceUrl = process.env.MEDIALOOPSTER_WEBSERVICE_URL
const medialoopsterWebserviceUser = process.env.MEDIALOOPSTER_WEBSERVICE_USER
const medialoopsterWebservicePassword = process.env.MEDIALOOPSTER_WEBSERVICE_PASSWORD

if (!fs.existsSync(uploadPath)) {
  console.log(`Upload path ${uploadPath} does not exist`);
  process.exit(1);
}

const app = express();
app.use(cors());

app.get('/', (req, res) => {
  res.send(`<h2>Upload Server</h2>
        <form action="/" enctype="multipart/form-data" method="post">
            <div>Title: <input type="text" name="title" /></div>
            <div>Production: <input type="text" name="production" /></div>
            <div>Description: <input type="text" name="description" /></div>
            <div>File: <input type="file" name="file" /></div>
            <input type="submit" value="Upload" />
        </form>`)
});

app.post('/', async (req, res, next) => {
  const form = formidable({
    keepExtensions: true,
    uploadDir: uploadPath,
    maxFileSize: maxFileSize,
    filename: function (name, ext) {
      const id = uuid4()
      return `${id}${ext.toLowerCase()}`
    }
  });

  await form.parse(req, (err, fields, files) => {
    console.log(fields);
    console.log(files);
    const file = files.file[0];

    if (err) {
      console.log('Error during file upload:', err);
      next(err);
    }

    const title = Array.isArray(fields.title) ? fields.title[0] : fields.title;
    const production = Array.isArray(fields.production) ? fields.production[0] : fields.production;
    const description = Array.isArray(fields.description) ? (fields.description[0] || '') : (fields.description || '');

    const metaFieldStore = {};
    Object.keys(fields).forEach(key => {
      metaFieldStore[key] = Array.isArray(fields[key]) ? fields[key][0] : fields[key];
    })
    metaFieldStore.original_filename = file.originalFilename;

    delete metaFieldStore.title;
    delete metaFieldStore.production;
    delete metaFieldStore.description;

    console.log(`title: ${title}`);
    console.log(`production: ${production}`);
    console.log(`description: ${description}`);
    console.log('meta_field_store:');
    console.log(metaFieldStore);
    console.log(`filename: ${file.originalFilename}`);
    console.log(`filetype: ${file.mimetype}`);
    console.log(`filepath: ${file.filepath}`);

    if (medialoopsterWebserviceUrl && medialoopsterWebserviceUser && medialoopsterWebservicePassword) {
      if (title && production) {
        axios({
          method: 'POST',
          url: medialoopsterWebserviceUrl,
          auth: {
            username: medialoopsterWebserviceUser,
            password: medialoopsterWebservicePassword
          },
          data: {
            production,
            type: 'video',
            move_asset: 'true',
            asset: {
              asset_meta: {
                name: title,
                description,
                approval: 1,
                path_file: file.filepath,
                meta_field_store: metaFieldStore
              }
            }
          },
          httpsAgent: new https.Agent({
            rejectUnauthorized: false
          })
        }).then(function () {
          const info = `${file.originalFilename}: OK`;
          console.log(info);
          res.json({ title, production, description, metaFieldStore, files });
        }).catch(function (error) {
          const info = `${file.originalFilename}: FAILED`;
          const status = error.response.status || 500;
          console.error(info);
          res.status(status).json({ info });
        })
      } else {
        console.log('Title and Production are not defined');
      }
    } else {
      console.log('medialoopster is not configured');
      res.json({ title, production, description, metaFieldStore, files });
    }
  })
});

const server = app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
})
server.requestTimeout = 0;
